﻿/*
* Copyright (c) 2012-2020 AssimpNet - Nicholas Woodfield
* Copyright (c) 2022 Tony Hoyle
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/


using Assimp.Unmanaged;
using System;
using System.Runtime.InteropServices;

namespace AssimpNet.Windows
{
    internal class WindowsRuntimePlatform : RuntimePlatform
    {
        public override UnmanagedLibraryImplementation CreateRuntimeLibraryImplementation(UnmanagedLibraryResolver resolver, string defaultLibName, Type[] unmanagedFunctionDelegateTypes)
        {
            [DllImport("kernel32.dll", CharSet = CharSet.Ansi, BestFitMapping = false, SetLastError = true, EntryPoint = "LoadLibrary")]
            static extern IntPtr WinNativeLoadLibrary(String fileName);

            try
            {
                WinNativeLoadLibrary("non-existent-dll-that-is-never-used.dll");

                return new UnmanagedWin32LibraryImplementation(defaultLibName, unmanagedFunctionDelegateTypes);
            }
            catch (DllNotFoundException)
            {
                // Continue with fallback.
            }

            [DllImport("api-ms-win-core-libraryloader-l2-1-0.dll", SetLastError = true, EntryPoint = "LoadPackagedLibrary")]
            static extern IntPtr WinUwpLoadLibrary([MarshalAs(UnmanagedType.LPWStr)] string libraryName, int reserved = 0);

            try
            {
                //If we're running in an UWP context, we need to use LoadPackagedLibrary. On non-UWP contexts, this
                //will fail with APPMODEL_ERROR_NO_PACKAGE, so fall back to LoadLibrary.
                WinUwpLoadLibrary("non-existent-dll-that-is-never-used.dll");

                return new UnmanagedUwpLibraryImplementation(defaultLibName, unmanagedFunctionDelegateTypes);
            }
            catch (DllNotFoundException)
            {
                // Continue with fallback.
            }

            throw new PlatformNotSupportedException(
                "Windows system detected, but neither Win32 LoadLibrary nor UWP LoadPackagedLibrary could be " +
                "called, which are necessary to load the Assimp DLL. Your version of Windows is likely not supported."
            );
        }

        public override Assimp.Unmanaged.Platform GetPlatform()
        {
            return Assimp.Unmanaged.Platform.Windows;
        }
    }
}