﻿/*
* Copyright (c) 2012-2020 AssimpNet - Nicholas Woodfield
* Copyright (c) 2022 Tony Hoyle
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/


using Assimp.Unmanaged;
using System;
using System.Runtime.InteropServices;

namespace AssimpNet.Linux
{
    internal class LinuxRuntimePlatform : RuntimePlatform
    {
        public override UnmanagedLibraryImplementation CreateRuntimeLibraryImplementation(UnmanagedLibraryResolver resolver, string defaultLibName, Type[] unmanagedFunctionDelegateTypes)
        {
            [DllImport("libc.so.6", EntryPoint = "dlerror")]
            static extern IntPtr libc6_dlerror();

            try
            {
                libc6_dlerror();

                return new UnmanagedLinuxLibc6LibraryImplementation(defaultLibName, unmanagedFunctionDelegateTypes);
            }
            catch (DllNotFoundException)
            {
                // Continue with fallback.
            }

            [DllImport("libdl.so", EntryPoint = "dlerror")]
            static extern IntPtr libdl_dlerror();

            try
            {
                //Recent versions of glibc include the dl* symbols in the main library, and the C library is pretty much
                //always present in applications. Older versions glibc don't include these symbols in libc.so, and
                //require loading libdl separately instead.
                libdl_dlerror();

                return new UnmanagedLinuxLibdlLibraryImplementation(defaultLibName, unmanagedFunctionDelegateTypes);
            }
            catch (DllNotFoundException)
            {
                // Continue with fallback.
            }

            throw new PlatformNotSupportedException(
                "Linux system detected, but neither libc.so.6 nor libdl.so contains symbol " +
                "'dlopen' necessary to load Assimp DLL. Check that either of these so files are " +
                "present on your (Linux) system and correctly expose this symbol."
            );
        }

        public override Assimp.Unmanaged.Platform GetPlatform()
        {
            return Assimp.Unmanaged.Platform.Linux;
        }
    }
}