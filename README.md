This is an unofficial AssimpNet 5.0.0 build adding support for extra platforms eg. Android and iOS.  If you're just building for Windows you probably want the official assimpnet package, not this one.

A .NET Wrapper for the Open Asset Import (Assimp) library. The library is able to import dozens of different 3D model formats (e.g. OBJ, FBX, GLTF, Collada) and export to several formats (e.g. OBJ, GLTF, Collada).

AssimpNet.Mobile.Core must be paired with an AssimpNet.Mobile.Native.<platform> package to provide the actual native binaries.

Ad the beginning of your applcation add a line:

AssimpNet.Platform.Init();

Before calling any assimp functions.


